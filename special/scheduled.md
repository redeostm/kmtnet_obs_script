# Special Feature: Scheduled Observations #

This README shows a brief explanation of how to write the KMTNet observation script for scheduled observations with specific observing times.

:warning: **WARNING** :warning:

KMTNet team does not guarantee the results of running any of the following features. Use these features at your own risk.


## Should I Try It? ##

Unlike the recommended setup that the exposure lines could be executed at any available time, 
the scheduled observation feature allows users to specify when the observation starts (`UTOBS`) and what is the tolerance for the observation time (`UTTOL`).
When enabled, a given exposure line is excuted only if the KMTNet system is **healthy and ready** at the expected observation time between `UTOBS` +/- `UTTOL`.
However, the line could be skipped if the KMTNet system is not available during the expected observation time period, 
either due to the health issue or conflict with another scheduled observation
(please see [How It Works](#how-it-works) for details).


In summary, you may need to apply the scheduled observation feature **ONLY WHEN**:

- Your targets **must** be observed **only at** a given specific time (e.g., extremely short-period transient events, or fast-moving objects such as near-earth objects (NEO) or near-earth asteroids (NEA)).

On the other hand, you **SHOULD NOT** using the scheduled observation either:

- Visiting the given field is more important than satisfying the specific observation time
- You want to make sure executing all your exposure lines in your script
- Number of frames per field is crucial for your obsrvation program 


## Format ##

When you apply the scheduled observations, your exposure lines will have the following format:
```
PROJID  LABEL  RA  DEC  COPT  IMGTYP  OBJECT  FILTER  EXPTIME  UTOBS  UTTOL
```
Compared to the general format (see [README.md](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/README.md)), 
two new columns `UTOBS` and `UTTOL` are added at the end as follows 
(see [special/sample/single.scheduled.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/special/sample/single.scheduled.osc)):

- `UTOBS`  : Universal Time (UT) to start the observation (``YYYY-MM-DD``T``HH:MM:SS`` or ``YYYY-MM-DD``T``HH:MM``).
	- Otherwise (usually '-') : The exposure line is not executed as scheduled and will be executed once whenever it is ready.
- `UTTOL`  : Tolerance for `UTOBS` in seconds.
	- Positive value greater than 30 : The scheduled observation tries to start observing at `UTOBS` +/- `UTTOL`.
        Non-integer value will be changed to the integer value.
		If it is impossible to start the observation in the given range, the exposure line is skipped.
    - Positive value less than 30 : `UTTOL` is automatically set to 30 seconds, and the scheduled observation tries to start observing at `UTOBS` +/- 30s. 
	- '0', negative value, or non-value (usually '-') : The exposure line is not executed as scheduled and will be executed once whenever it is ready.
    
    
## How It Works ##

When the observing script reaches an exposure line containing proper values of `UTOBS` and `UTTOL`, 
the following will happen depending on the current expected observing time (_T_): 

- `UTOBS` - `UTTOL` < _T_ < `UTOBS` + `UTTOL` : The given exposure line will be excuted once. After the execution, it will move to the next exposure line.
- _T_ < `UTOBS` - `UTTOL` : It will wait without exposure. It may wait with additional exposure if the time permits.
- _T_ > `UTOBS` + `UTTOL` : It will depend on the relation among _T_, `UTOBS` + `UTTOL` at the current line (=_T1_) and `UTOBS` - `UTTOL` at the next line (=_T2_ > _T1_).
    - _T_ - _T1_ < _T2_ - _T_ : It will execute the current exposure line.
    - _T_ - _T1_ > _T2_ - _T_ : It will skip the current exposure line and move to the next line.


## Example Scripts ##

- [special/sample/single.scheduled.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/special/sample/single.scheduled.osc) : A single scheduled exposure.