##########################################################################################
# A sample KMTNet observation script for a single scheduled exposure
#
# WARNING: KMTNet team does not guarantee the results of running the scheduled exposures. 
#          Use this feature at your own risk.
#
# This sample shows a single exposure with the field center
# (RA,DEC) = (11:08:28.00, +05:31:20.0) (hh:mm:ss.ss, +dd:mm:ss.s)
# with 100 second exposure by using the Johnson R-filter,
# that starts the exposure at the desired universal time (UT)
# 2021-04-05T09:33:40, with the 50-second tolerance at the starting time.
#
# See REAMDE.md and sample/single.ontime.osc for the basic syntax.
# See special/observing_time.md for the scheduled exposure.
#
# Initial script made by Dr. Sangmok Cha, Jul. 2nd, 2021
# Minor correction by Dr. Sungwook E. Hong, Nov. 10th, 2023
###########################################################################################

# First line as the exposure line that defines the exposure.
# This exposure is included in the observation program called DEEPS (PROJID = DEEPS),
# and the observer labeled it as LS2021038(R) (LABEL = LS2021038(R)).
# It sets (RA,DEC) = (11:08:28.00, +05:31:20.0) as the field center without applying any correction (COPT = 0).
# It targets the actual scientific objects (IMGTYP = OBJECT), which will be recorded as E00166 in the FITS header (OBJECT = E00166).
# It takes an exposure with Johnson-R filter (FILTER = R) and 100-second exposure time (EXPTIME = 100).
# The observation is scheduled to start at the universal time (UT) 2021-04-05T09:33:40 (UTOBS = 2021-04-05T09:33:40)
# by allowing 50-second tolearance at the starting time (UTOBS = 50).
#
# One can add as many exposure lines with the same format as desired.

 # PROJID  LABEL         RA          DEC          COPT  IMGTYP  OBJECT   FILTER  EXPTIME  UTOBS                UTTOL    # comments
   DEEPS   LS2021038(R)  11:08:28.00 +05:31:20.0  0     OBJECT  E00166   R       100      2021-04-05T09:33:40  50 


# Second line as the command line that stops the exposure.

+ostop

# Usually, additional messages to the observer or operator are given as comments at the end of the script as follows.
# They include the desired operation period and how many loops one should perform, etc.

# EOF