# KMTNet Observation Script Manual #

This README shows a brief explanation of how to write the KMTNet observation script.


## Format ##

A KMTNet observation script has `.osc` extension in its filename. 
Each KMTNet observation script consists of multiple lines separated by either LF (`0x0A`) or CR/LF (`0x0D0A`). 
Lines having more than 256 ASCII characters cause an error and are not executed. 
Each line consists of multiple words separated by blanks (`0x20`), and similar to many modern script languages, any number of blanks can exist between two words.

Three types of lines are possible in the KMTNet observation scripts --- exposure lines, command lines, and comments.


### Comments ###

Any lines or parts of lines that start with the ASCII character '#' are comments. 
Like many modern script languages, comments are used only for helping the observers are not executed.

Example:
```python
# This entire line is a comment.

+ostart 1  # This part is a comment.
```


### Exposure Lines ###

Exposure lines define the CCD exposures in the KMTNet.
Each exposure line corresponds to a single exposure and creates an individual MEF file as an output.

Exposure lines consist of multiple columns with the following format:
```
PROJID  LABEL  RA  DEC  COPT  IMGTYP  OBJECT  FILTER  EXPTIME
```
The definitions of each column are as follows (see [sample/single.ontime.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/sample/single.ontime.osc)):

- `PROJID` : Project ID about the observation program.
- `LABEL`  : Description of an exposure line, not recorded in FITS header, e.g., index#, serial#, field ID, or any information about the exposure.
	- It is used only for helping observers and/or operators to understand the structure of the script.
- `RA/DEC` : J2000 RA/DEC coordinate (``HH:MM:SS.SS`` / ``+DD:MM:SS.S``).
	- By default, it directs the center of the field-of-view (FOV), where a gap between CCDs exists. 
		Therefore, one should make sure that objects have sufficient offsets from the given (RA, DEC).
	- '-' : Does not move the telescope.
		One can use this option for bias / domeflat exposures, for example.
		However, one SHOULD NOT use this option for taking multiple exposures at a fixed (RA, DEC). 
		For such purpose, one should enter the same (RA, DEC) repeatedly for each exposure line instead.
- `COPT`   : pointing correction option 
	- '-' / '0' :  No correction
	- '1' : BLG offset correction
	- 'k' / 'm' / 't' / 'n' :  RA/DEC = K/M/T/N CCD center
- `IMGTYP` : Image type (object / bias / dark / flat / domeflat / sky)
- `OBJECT` : Object name to record in FITS header
- `FILTER` : Filter name
	- 'I' / 'R' / 'V' / 'B' : Johnson-Cousins filter set
	- 'z' / 'i' / 'r' / 'z' : SDSS filter set
	- '1' / '2' / '3' / '4' : Custom filters defined by their slide numbers
	- 'Ha' / 'M000' : H-alpha / Mid-band filters
	- 'no' / 'n' / 'N' / '0' : No filter
- `EXPTIME` : Exposure time in seconds.
	- The mininum value is 0.1, except the bias image is set to zero regardless of the input value.

All columns except the `OBJECT` are case-insensitive.

Example:
```python
# Define an exposure in the BLG program with (RA,DEC) = (17:54:24, -31:08:00)
# by using Johnson-I filter with 60-second exposure time.

BLG   NORMAL-1015-I  17:54:24 -31:08:00  1  object  blg01  I  60
```

### Command lines ###

Command lines are used to repeat, stop, pause, and resume the exposure lines. 
All command lines start with the ASCII character '+', while the following texts follow the same syntax to the OBSAgent.

- `+ostart <line #>` : Go to the given line number and start execution.
	- It is used for repeating multiple exposures.
	- There is no option for specifying the number of loops in this command.
		Instead, the observer should let the operator know the number with an additional comment.
- `+ostop` : Stop the script.
	- To restart the script, input `+ostart <line #>`
- `+opause` : Pause the script.
	- To resume the script from the next exposure line, input `+resume`.
	- To resume/restart the script from a certain exposure line, input `+ostart <line #>`.
	
Note that line numbers are counted from both exposure and command lines, not comments or blank lines.

Example:
```python
+ostart 1

## NOTE : Start at UT 00:00 - Finish at 00:00
## NOTE : Repeat the observation 40 times.
```

## Special Features ##

:warning: **WARNING** :warning:

KMTNet team does not guarantee the results of running any of the following features. Use these features at your own risk. 


### Scheduled Observations ###

Normally, the submitted scripts are executed at any time that the telescope can target the field. 
However, if specifying the observing time is crucial for the program (e.g., target-of-opportunity observations),
you may consider applying the scheduled observation feature to your script.
See [special/scheduled.md](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/special/scheduled.md) for details.


## Example Scripts ##

### Basic Examples ###

- [sample/single.ontime.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/sample/single.ontime.osc) : A single exposure.
- [sample/multiple.repeated.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/sample/multiple.repeated.osc) : Two sets of repeated several times.

### Comprehensive Examples: Dithering ###

- [sample/dither.arcsec.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/sample/dither.arcsec.osc) : Five sets of exposures that dither a given field position with a fixed angular distance.
- [sample/dither.ccd.osc](https://bitbucket.org/redeostm/kmtnet_obs_script/src/master/sample/dither.ccd.osc) : Five sets of exposures that dither a given field position as FOV center and K/M/T/N-CCD centers.
